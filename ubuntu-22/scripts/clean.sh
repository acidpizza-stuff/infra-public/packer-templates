#!/usr/bin/env bash

set -euo pipefail

# Stop services for cleanup
sudo service rsyslog stop

# Remove the local machine id prevent the possibility of machines having
# duplicate identities post cloning operations
sysd_id="/etc/machine-id"
dbus_id="/var/lib/dbus/machine-id"

# Remove machine id
sudo truncate -s 0 ${sysd_id}
# Remove the machine-id file under /var/lib/dbus if it is not a symlink
if [[ -e ${dbus_id} && ! -h ${dbus_id} ]]; then
    rm -f ${dbus_id}
fi

# Remove logs and all temp. files
sudo rm -rf /var/log/* || true
sudo rm -rf /tmp/*
sudo rm -rf /var/tmp/*

# Ensure new ssh server keys are regenerated for each VM
# Missing host keys will be regenerated on startup by cloudinit
sudo rm -rf /etc/ssh/ssh_host_*

#cleanup persistent udev rules
if [ -f /etc/udev/rules.d/70-persistent-net.rules ]; then
  sudo rm /etc/udev/rules.d/70-persistent-net.rules
fi

# remove static ip network configuration
sudo rm -f /etc/netplan/*
# cat <<EOF | sudo tee /etc/netplan/01-netcfg.yaml
# network:
#   version: 2
#   renderer: networkd
# EOF

# Reset hostname
cat /dev/null | sudo tee /etc/hostname

# Cleanup apt
sudo apt-get clean

# Revoke passwordless sudo
#sudo rm /etc/sudoers.d/ubuntu

# Remove shell history
echo > ~/.bash_history
history -cw

exit 0