#!/bin/sh

set -eu 

# This script extends the lvm from the first disk to use all free space
# The first disk is typically /dev/vda3
# To specify another disk, run with the appropriate arguments.
# Eg. ./auto_resize_vda.sh </dev/sda> <4> <my-vg-name> <my-lv-name>

# $1 - disk drive (default = /dev/vda)
DISK_DRIVE=${1:-/dev/vda}
# $2 - partition number (default = 3)
PARTITION_NUM=${2:-3}
# $3 - vg name (default = ubuntu-vg)
VG_NAME=${3:-ubuntu-vg}
# $4 - lv name (default = ubuntu-lv)
LV_NAME=${4:-ubuntu-lv}

sudo growpart ${DISK_DRIVE} ${PARTITION_NUM}
sudo pvresize ${DISK_DRIVE}${PARTITION_NUM}
lvextend -l +100%FREE -r /dev/${VG_NAME}/${LV_NAME}
