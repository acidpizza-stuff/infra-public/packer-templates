## Usage

From the desired build folder, create the secrets file and populate the secrets accordingly.

```bash
cd ubuntu-22/
cp secrets.hcl.sample secrets.hcl
```

Perform the build.

```bash
# Perform build
make ubuntu22

# The terminal will become spoiled (not sure why).
# Reset terminal
reset
```


## Prerequisites

- Client VM must have external connectivity (bridged mode) to run HTTP Server.


## Notes

- IP seems to be set correctly on autoinstall. Don't need to set on kernel boot parameters. Setting network configuration on kernel boot parameters doesn't seem to work also.
- Need qemu agent to obtain network information in guest VM. Setting `qemu_agent=true` is insufficient. Need to install `qemu-guest-agent` package via cloudinit userdata. It will be automatically enabled and run via systemd.
- ssh_pty needed for sudo to work.
- Need passwordless sudo to run scripts. Restore password at end of script.


## Set md5 Checksum for ISO

```bash
# Linux
md5sum ubuntu-20.04.3-live-server-amd64.iso

# Windows
Get-FileHash .\ubuntu-22.04.4-live-server-amd64.iso -Algorithm MD5

# iso_checksum="md5:8df52f27204c37a50a169989fb019188"
```


## Setting Password

```bash
mkpasswd -m SHA-512 --rounds=4096
```


## Cloud-Init

- Refer to: https://pve.proxmox.com/wiki/Cloud-Init_Support and https://pve.proxmox.com/wiki/Cloud-Init_FAQ
- Perform `sudo cloud-init clean` to allow cloud-init to run again when spawning VM from this template.
- Proxmox generates an ISO image to pass the cloud-init data to the VM. Hence, the VM needs a CD-ROM drive.
- Serial console needed. Can be enabled on grub command line.

### Regenerate SSH Keys

Reference: https://serverfault.com/questions/1116547/why-the-rhel8-system-do-not-generate-ssh-host-keys-automatically-when-missing

New versions of cloud-init will block ssh server keys from being regenerated if missing. Need to clean cloud init cache so that it will run again and regenerate ssh keys.


## Proxmox Plugin

From [version 1.9.0](https://github.com/hashicorp/packer/releases/tag/v1.9.0), the proxmox plugin will not be bundled with Packer anymore. With the `packer.required_plugins` block, the proxmox plugin can be installed via `packer init`.


## Troubleshooting

```bash
PACKER_LOG=1 packer build -var-file secrets.hcl .
```

### Boot Stuck at "Reached target Host and Network Name Lookups"

Reference:
- [Error message means provisioning server cannot reach packer server](https://github.com/systemd/systemd/issues/17686)
- [Port forwarding instructions](https://stackoverflow.com/a/66485783)

This happened because the server was unable to reach the host that was running Packer. Enabling WSL2 caused local VMs to use NAT networking and hence the packer HTTP server was not accessible externally.

Need to enable port forwarding to the VM/WSL2 host. Need to fix the http ip and port.
- Set `var.http_ip` in `secrets.hcl` as the windows host IP. The default value of `{{ .HTTPIP }}` will only get the guest IP which is also not accessible externally.

```powershell
# Find free port and update http_port_min and http_port_max appropriately
netstat -ano | Select-String 8100

# set the http port and linux guest IP
$port=8100
$ip="172.28.215.133"

# Remember to remove the firewall and port forwarding rules after image is built!
netsh advfirewall firewall add rule name="packer" dir=in action=allow protocol=TCP localport=$port
netsh advfirewall firewall show rule name=packer
netsh advfirewall firewall delete rule name=packer

netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 connectaddress=$ip listenport=$port connectport=$port
netsh interface portproxy show all
netsh interface portproxy delete v4tov4 listenaddress=0.0.0.0 listenport=$port
```

> Seems like all these are not necessary as setting VM networking to `bridged` still works. Using `nc -l` and `nc -zv` tests leads to `inverse host lookup failed: unknown host` error, but otherwise connections are still successful.
