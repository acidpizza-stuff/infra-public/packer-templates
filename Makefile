# -------------------------------------------------------------------------
# Variables
PACKER_VERSION = 1.9.4

# -------------------------------------------------------------------------
# Ensure Makefile recognizes bash syntax rather than the default sh shell
SHELL := /bin/bash
# -------------------------------------------------------------------------
# Automatically print all commands with help description denoted after ## in same line
.DEFAULT_GOAL := help

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; \
	printf "\033[33mUsage:\033[0m\n  make <target>\n\n", "" } \
	/^[a-z0-9A-Z_-]+:.*?##/ { printf "  \033[32m%-20s\033[0m %s\n", $$1, $$2 } \
	/^##/ { printf "\033[33m%s\033[0m\n", substr($$0, 4) } \
	END { printf "\n"}' $(MAKEFILE_LIST)
# -------------------------------------------------------------------------

## Build
.PHONY: ubuntu20
ubuntu20: ## Build ubuntu 20.04 image
	@docker run --rm -t --entrypoint /bin/bash -v ${PWD}:/code -w /code --network=host hashicorp/packer:$(PACKER_VERSION) \
		-c "cd ubuntu-20/ && packer init . && packer build -var-file secrets.hcl -force ."

.PHONY: ubuntu22
ubuntu22: ## Build ubuntu 22.04 image
	@docker run --rm -t --entrypoint /bin/bash -v ${PWD}:/code -w /code --network=host hashicorp/packer:$(PACKER_VERSION) \
		-c "cd ubuntu-22/ && packer init . && packer build -var-file secrets.hcl -force ."

## Plugins
.PHONY: check-plugins
check-plugins: ## Check installed plugins (no effect in docker)
	@docker run --rm -t hashicorp/packer:$(PACKER_VERSION) plugins installed

## Shell
.PHONY: shell
shell: ## Get shell access to docker container with packer
	@docker run --rm -it --entrypoint /bin/bash -v ${PWD}:/code -w /code hashicorp/packer:$(PACKER_VERSION)
