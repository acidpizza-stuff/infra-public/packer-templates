#############################################################
# Proxmox Config
#############################################################
variable "proxmox_hostname" {
  description = "Proxmox host address (e.g. https://192.168.1.1:8006)"
  type = string
  default = "https://pve.lan:8006"
}

variable "proxmox_username" {
  description = "Proxmox username (e.g. root@pam)"
  type = string
  default = "root@pam"
}

variable "proxmox_password" {
  description = "Proxmox password"
  type = string
}

variable "proxmox_node_name" {
  description = "Proxmox node to start VM on during build"
  type = string
  default = "pve"
}

variable "proxmox_insecure_skip_tls_verify" {
  description = "Skip TLS verification?"
  type = bool
  default = true
}