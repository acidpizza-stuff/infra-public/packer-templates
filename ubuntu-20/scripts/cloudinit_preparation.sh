#!/usr/bin/env bash

set -euo pipefail

# https://pve.proxmox.com/wiki/Cloud-Init_FAQ#Creating_a_custom_cloud_image

# Setup a serial terminal (tty1, ttyS0)
# Disable predicatable network interface names (revert to eth0)
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT="console=tty1 console=ttyS0 net.ifnames=0"/1' /etc/default/grub
sudo update-grub

# Allow network configuration
sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg

# Proxmox VE boot-time optimization (don't need to do this at it is done by default in ubuntu image at /etc/cloud/cloud.cfg.d/90_dpkg.cfg)
# echo "datasource_list: [ NoCloud, ConfigDrive ]" | sudo tee /etc/cloud/cloud.cfg.d/99-pve.cfg

# Reset cloud-init to allow it to run again (eg. to regenerate SSH server keys)
sudo cloud-init clean
