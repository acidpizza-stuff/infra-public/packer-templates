#!/usr/bin/env bash

set -euo pipefail

# Get latest updates now during template creation
# package_upgrade option in cloudinit will only upgrade on first boot of VM
sudo DEBIAN_FRONTEND=noninteractive apt update
sudo DEBIAN_FRONTEND=noninteractive apt -y full-upgrade
